package ru.document.flow.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.document.flow.dao.model.UserModel;
import ru.document.flow.dao.model.UserRole;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class UserDetailsServiceImplTest {

    private UserService userService = Mockito.mock(UserService.class);
    private UserDetailsService userDetailsService = new UserDetailsServiceImpl(userService);

    @Test
    void loadUserByUsername_throwException_userNotFound() {
        assertThrows(UsernameNotFoundException.class, () -> {
            userDetailsService.loadUserByUsername("Vasaya");
        });
    }

    @Test
    void loadUserByUsername_ok(){
        UserModel user = new UserModel();
        user.setUserId(1L);
        user.setUserName("Ivan");
        user.setPassword("$2a$04$eDpY6nmRsqiodjkrrfEtzOOuJwLkexjKAC3XYWzK22.6lij8urX3O");
        user.setUserRole(UserRole.USER);
        when(userService.findByUserName("Ivan")).thenReturn(user);
        assertEquals(user.getUserName(),userDetailsService.loadUserByUsername("Ivan").getUsername());
    }

}