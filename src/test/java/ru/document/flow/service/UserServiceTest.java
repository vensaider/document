package ru.document.flow.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.document.flow.dao.model.UserModel;
import ru.document.flow.dao.model.UserRole;
import ru.document.flow.dao.repository.UserRepository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private UserRepository userRepository = Mockito.mock(UserRepository.class);

    private UserService userService = new UserService(userRepository);

    @Test
    void findByUserName() {
        UserModel user = new UserModel();
        user.setUserId(1L);
        user.setUserName("Vasay");
        user.setPassword("$2a$04$eDpY6nmRsqiodjkrrfEtzOOuJwLkexjKAC3XYWzK22.6lij8urX3O");
        user.setUserRole(UserRole.USER);
        when(userRepository.findByUserName("Vasay")).thenReturn(user);

        assertEquals(user,userService.findByUserName("Vasay"));
    }

}