package ru.document.flow.service;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.document.flow.dao.model.DocumentModel;
import ru.document.flow.dao.repository.DocumentRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

class DocumentServiceImplTest {

    private DocumentRepository documentRepository = Mockito.mock(DocumentRepository.class);

    private DocumentService documentService = new DocumentService(documentRepository);

    @Test
    void getAllDocument() {
        DocumentModel documentModel = documentModel();
        List<DocumentModel> documentModelList = new ArrayList<>();
        documentModelList.add(documentModel);
        when(documentRepository.findAll()).thenReturn(documentModelList);

        assertEquals(documentModel, documentService.getAllDocument().get(0));
        assertEquals(1, documentService.getAllDocument().size());
    }

    @Test
    void getDocumentById() {
        DocumentModel documentModel = documentModel();
        Optional<DocumentModel> documentModelOptional = Optional.of(documentModel);
        when(documentRepository.findById(1L)).thenReturn(documentModelOptional);

        assertEquals(documentModel, documentService.getDocumentById(1L).get());
    }

    @Test
    void addDocument() {
        DocumentModel documentModel = documentModel();
        when(documentRepository.saveAndFlush(documentModel)).thenReturn(documentModel);

        assertEquals(documentModel, documentService.addDocument(documentModel));
    }

    @Test
    void updateDocument() {
        DocumentModel documentModel = documentModel();
        DocumentModel documentModelUpdate = new DocumentModel();
        documentModelUpdate.setDocumentId(1L);
        documentModelUpdate.setName("TestDocumentUpdate");
        documentModelUpdate.setDocumentNumber(350L);
        documentModelUpdate.setAmount(new BigDecimal(6000));
        documentModelUpdate.setDocumentDate(new Date());
        when(documentRepository.save(documentModel)).thenReturn(documentModel);
        documentModel.setDocumentDate(documentModelUpdate.getDocumentDate());
        documentModel.setDocumentNumber(documentModelUpdate.getDocumentNumber());
        documentModel.setAmount(documentModelUpdate.getAmount());
        documentModel.setName(documentModelUpdate.getName());

        assertEquals(documentModelUpdate, documentService.updateDocument(documentModel));
    }

    @Test
    void deleteDocument() {
        doNothing().doThrow(new IllegalArgumentException()).when(documentRepository).deleteById(1L);
    }

    @Test
    void getDocumentModelByDocumentNumber() {
        DocumentModel documentModel = documentModel();
        when(documentRepository.findDocumentModelByDocumentNumber(1L)).thenReturn(documentModel);

        assertEquals(documentModel, documentService.getDocumentModelByDocumentNumber(1L));
    }

    private DocumentModel documentModel() {
        DocumentModel documentModel = new DocumentModel();
        documentModel.setDocumentId(1L);
        documentModel.setName("TestDocument");
        documentModel.setDocumentNumber(340L);
        documentModel.setAmount(new BigDecimal(5000));
        documentModel.setDocumentDate(new Date());
        return documentModel;
    }
}