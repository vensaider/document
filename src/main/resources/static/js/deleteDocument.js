function deleteDocument(documentNumber) {
    const xhrDeleteDocument = new XMLHttpRequest();
    xhrDeleteDocument.open("POST", "/delDoc", true);
    xhrDeleteDocument.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrDeleteDocument.onreadystatechange = function () {
        if(xhrDeleteDocument.status == 403){
            window.location.href = "/403";
        }
        if (xhrDeleteDocument.readyState != 4 && xhrDeleteDocument.status == 200) {
            const documentJson = JSON.parse(xhrDeleteDocument.responseText);
            console.log(documentJson);
            const documentNumberJson = documentJson.documentNumber;


            let table = document.getElementById("document-table");

            for (let i = 0; i < table.rows.length; i++) {
                for (let j = 0; j < table.rows[i].cells.length; j++) {
                    if (table.rows[i].cells[j].firstChild.nodeValue == documentNumberJson) {
                        table.deleteRow(i);
                        break;
                    }
                }
            }
        }
    };
    xhrDeleteDocument.send(JSON.stringify({
        documentNumber: documentNumber
    }));
}