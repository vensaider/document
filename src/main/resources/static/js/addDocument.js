function addDocument() {

    const xhrAddDocument = new XMLHttpRequest();
    xhrAddDocument.open("POST", "/addDoc", true);
    xhrAddDocument.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhrAddDocument.onreadystatechange = function () {
        if(xhrAddDocument.status == 403){
            window.location.href = "/403";
        }
        if (JSON.parse(xhrAddDocument.responseText).documentNumber == undefined ){
            alert("Все поля должны быть заполнены!")
            return;
        }
        if (JSON.parse(xhrAddDocument.responseText).documentNumber != 0 & JSON.parse(xhrAddDocument.responseText).amount == undefined ){
            alert("Такой номер документа уже есть, введите другой!")
            return;
        }
        if (xhrAddDocument.readyState != 4 && xhrAddDocument.status == 200) {
            const documentJson = JSON.parse(xhrAddDocument.responseText);
            const dateDocument = new Date(documentJson.documentDate);
            const formatedDate = dateDocument.getFullYear() + "-" + "0" + (dateDocument.getMonth() + 1) + "-" + dateDocument.getDate() ;
            documentJson.documentDate = formatedDate;
            const documentNumberJson = documentJson.documentNumber;
            delete documentJson["documentId"];
            delete documentJson["name"];
            const partDocumentJson = {};
            partDocumentJson.Update = "<div class=\"button-icon-conteiner\"><a class=\"small-button\"><i class=\"fa fa-pencil fa-2x\"></i></a>\n" +
                "</div>";
            documentJson.Delete = "<form onclick=\"deleteDocument();return false;\" action=\"/delDoc/\"  method=\"post\"  accept-charset=utf-8><div class=\"button-icon-conteiner\"><button id=\"#button" + documentNumberJson + "\" value=\"" + documentNumberJson + "\" onclick=\"getNumber(this.id);\"><i class=\"fa fa-bitbucket fa-2x\"></i></button></div></form>";
            const fullDocumentJson = {...partDocumentJson,...documentJson};
            let documents = [];
            documents.push(fullDocumentJson);
            let col = [];

            for (let i = 0; i < documents.length; i++) {
                for (let key in documents[i]) {
                    if (col.indexOf(key) === -1) {
                        col.push(key);
                    }
                }
            }
            let table = document.getElementById("document-table");
            let tr = table.insertRow(-1);                   // TABLE ROW.

            for (let i = 0; i < col.length; i++) {
                let th = document.createElement("th");      // TABLE HEADER.
                th.innerHTML = col[i];
            }
            // ADD JSON DATA TO THE TABLE AS ROWS.
            for (let i = 0; i < documents.length; i++) {

                tr = table.insertRow(-1);

                for (let j = 0; j < col.length; j++) {
                    let tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = documents[i][col[j]];
                    if (i >= 2) {

                    }
                }
            }
        }
    };
    xhrAddDocument.send(JSON.stringify({
        name: document.getElementById("name").value,
        documentNumber: document.getElementById("number").value,
        amount: document.getElementById("amount").value
    }));
    clear();
}