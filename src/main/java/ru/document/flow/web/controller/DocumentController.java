package ru.document.flow.web.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.document.flow.dao.model.DocumentModel;
import ru.document.flow.service.DocumentService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Controller
@RequiredArgsConstructor
public class DocumentController {

    private final DocumentService documentService;

    private static final Logger logger = LoggerFactory.getLogger(DocumentController.class);

    @GetMapping(value = "/document")
    public String getAllDocument(Model model) {
        List<DocumentModel> documentModels = documentService.getAllDocument();
        logger.info("Get all documents size " + documentModels.size());
        model.addAttribute("documents", documentModels);
        return "document";
    }

    @PostMapping(value = "/addDoc", produces = "application/json")
    public @ResponseBody
    DocumentModel addDocument(@RequestBody Map<String, String> json){
        if (json.get("documentNumber").equals("") ||
                json.get("name").equals("") ||
                json.get("amount").equals("")){
            logger.debug("Not all fields are filled");
            return new DocumentModel();
        }
        DocumentModel documentModel = documentService.checkDocumentModel(Long.valueOf(json.get("documentNumber")),new BigDecimal(json.get("amount")),json.get("name"));
        logger.info("Document is save in base " + documentModel);
        return documentModel;
    }

    @PostMapping(value = "/delDoc", produces = "application/json")
    public @ResponseBody DocumentModel deleteDocument(@RequestBody Map<String, String> json){
        DocumentModel documentModel = documentService.getDocumentModelByDocumentNumber(Long.parseLong(json.get("documentNumber")));
        documentService.deleteDocument(documentModel.getDocumentId());
        logger.info("Document has by delete " + documentModel);
        return documentModel;
    }
}
