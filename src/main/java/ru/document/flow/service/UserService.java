package ru.document.flow.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.document.flow.dao.model.UserModel;
import ru.document.flow.dao.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {


    private final UserRepository userRepository;

    public UserModel findByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
}
