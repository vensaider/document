package ru.document.flow.service;


import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.document.flow.dao.model.DocumentModel;
import ru.document.flow.dao.repository.DocumentRepository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DocumentService {

    private static final Logger logger = LoggerFactory.getLogger(DocumentService.class);

    private final DocumentRepository documentRepository;

    public List<DocumentModel> getAllDocument() {
        return documentRepository.findAll();
    }

    public Optional<DocumentModel> getDocumentById(long documentId) {
        return documentRepository.findById(documentId);
    }

    public DocumentModel addDocument(DocumentModel document) {
        return documentRepository.saveAndFlush(document);
    }

    public DocumentModel updateDocument(DocumentModel document) {
        return documentRepository.save(document);
    }

    public void deleteDocument(long documentId) {
        documentRepository.deleteById(documentId);
    }

    public DocumentModel getDocumentModelByDocumentNumber(Long number) {
        return documentRepository.findDocumentModelByDocumentNumber(number);
    }

    public DocumentModel checkDocumentModel(Long documentNumber, BigDecimal amount,String name){
        Optional<DocumentModel> documentModelOptional = Optional.ofNullable(
                getDocumentModelByDocumentNumber(documentNumber));
        if (documentModelOptional.isPresent()){
            if (documentNumber.equals(documentModelOptional.get().getDocumentNumber())){
                DocumentModel documentModel = new DocumentModel();
                documentModel.setDocumentNumber(documentNumber);
                logger.debug("This document number already exist in base " + (documentNumber));
                return documentModel;
            }
        }
        DocumentModel documentModel = new DocumentModel();
        documentModel.setName(name);
        documentModel.setDocumentNumber(documentNumber);
        documentModel.setAmount(amount);
        documentModel.setDocumentDate(new Date());
        addDocument(documentModel);
        return documentModel;
    }
}
