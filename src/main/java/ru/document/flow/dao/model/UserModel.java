package ru.document.flow.dao.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "user_tbl")
@Data
public class UserModel {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long userId;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "user_passwd")
    private String password;

    @Column(name = "account_type")
    @Enumerated(EnumType.STRING)
    private UserRole userRole;
}