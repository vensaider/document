package ru.document.flow.dao.model;


import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "document_tbl")
@Data
public class DocumentModel {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long documentId;

    @Column(name = "document_name")
    private String name;

    @Column(name = "document_number")
    private Long documentNumber;

    @Column(name = "document_date")
    @Temporal(TemporalType.DATE)
    private Date documentDate;

    @Column(name = "amount")
    private BigDecimal amount;
}

