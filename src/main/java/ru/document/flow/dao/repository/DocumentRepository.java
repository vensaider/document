package ru.document.flow.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.document.flow.dao.model.DocumentModel;

@Repository
public interface DocumentRepository extends JpaRepository<DocumentModel,Long> {
    DocumentModel findDocumentModelByDocumentNumber(Long number);
}
