package ru.document.flow.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.document.flow.dao.model.UserModel;

@Repository
public interface UserRepository extends JpaRepository<UserModel,Long> {
    UserModel findByUserName(String userName);
}
